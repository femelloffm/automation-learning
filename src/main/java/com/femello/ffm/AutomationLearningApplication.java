package com.femello.ffm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutomationLearningApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutomationLearningApplication.class, args);
    }

}
