package com.femello.ffm.repositories;

import com.femello.ffm.models.Person;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class PersonRepository {
    public List<Person> findAll() {
        return Collections.singletonList(new Person("Buffy Summers", 16));
    }
}
