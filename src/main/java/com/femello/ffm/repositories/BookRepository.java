package com.femello.ffm.repositories;

import com.femello.ffm.models.Book;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookRepository {
    public List<Book> findAll() {
        Book firstBook = new Book("Orgulho e Preconceito", "Jane Austen", 1813);
        Book secondBook = new Book("A Metamorfose", "Franz Kafka", 1915);
        return List.of(firstBook, secondBook);
    }

    public Book save(Book book) {
        return book;
    }
}
