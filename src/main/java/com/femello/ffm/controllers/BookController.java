package com.femello.ffm.controllers;

import com.femello.ffm.dto.BookInput;
import com.femello.ffm.models.Book;
import com.femello.ffm.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/books")
public class BookController {
    private final BookService bookService;

    @Autowired
    public BookController(final BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public ResponseEntity<List<Book>> getBooksRequest() {
        return ResponseEntity.ok(bookService.findAllBooks());
    }

    @PostMapping
    public ResponseEntity<Book> createBook(@RequestBody BookInput bookInput) {
        return ResponseEntity.ok(bookService.createBook(bookInput));
    }
}
