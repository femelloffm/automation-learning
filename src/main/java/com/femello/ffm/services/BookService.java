package com.femello.ffm.services;

import com.femello.ffm.dto.BookInput;
import com.femello.ffm.models.Book;
import com.femello.ffm.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    private final BookRepository bookRepository;

    @Autowired
    public BookService(final BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> findAllBooks() {
        return bookRepository.findAll();
    }

    public Book createBook(BookInput bookInput) {
        Book bookToSave = new Book(bookInput.getTitle(), bookInput.getAuthor(), bookInput.getReleaseYear());
        return bookRepository.save(bookToSave);
    }
}
