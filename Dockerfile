FROM gradle:8.6.0-jdk11-alpine AS builder
WORKDIR /app
COPY build.gradle settings.gradle ./
COPY src/ src/
RUN gradle clean build --no-daemon -x test

FROM amazoncorretto:11-alpine3.19-jdk
WORKDIR /app
COPY --from=builder /app/build/libs/automation-learning-0.0.1-SNAPSHOT.jar app.jar
CMD java -jar app.jar